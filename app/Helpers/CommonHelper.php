<?php
namespace App\Helpers;

class CommonHelper
{
    public static function curlCall($url, $type = null, $postfields = null)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        if ($type == 'post') {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    public static function apiHeaders()
    {
        return [
            'Content-Type: application/json',
            'Accept: application/vnd.ibex.v1+json',
            'Authorization: Bearer ' . json_decode(cache('auth'))->access_token
        ];
    }

    public static function get($url)
    {
//        dd(self::apiHeaders());
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_HTTPHEADER, self::apiHeaders());
        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }
}