<?php

use App\Helpers\CommonHelper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('oauth/token', function (Request $request) {
    $payload = [
        'client_id' => 'c47be01f75872d14',
        'client_secret' => 'HfYU0XkNJ+XYHcTXukIzJ1SbjTrUX2IhL4r48DxJnq3B1fHq3CgS/SvzHQtB2PeS',
        'code' => $request->code,
        'grant_type' => "authorization_code"
    ];
    $result = CommonHelper::curlCall('http://api-demo.seekom.com/oauth/token', 'post', $payload);

    Cache::put('auth', $result, 86400);



    return response([
        'code' => 200,
        'message' => "Authenticated",
        'authorization' => 'test'
    ]);
});

Route::get('/properties-metadata', function () {
    return CommonHelper::get('http://api-demo.seekom.com/properties-metadata');
});

Route::get('/properties', function () {
    $result = CommonHelper::get('http://api-demo.seekom.com/properties');
    $properties = json_decode($result)->data;

    $data = [];
    $faker = Faker\Factory::create();
    foreach($properties as $property) {
        $property->image = $faker->imageUrl(360, 206, 'city');
        $data[] = $property;
    }

    return response([
        'code' => 200,
        'message' => 'properties list',
        'data' => $data
    ]);
});

