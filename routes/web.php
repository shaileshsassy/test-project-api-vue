<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use GuzzleHttp\Client;
use Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});


Route::post('token', function (Request $request) {
    $client = new Client();
    $result = $client->post('http://api-demo.seekom.com/oauth/token', [
        'form_params' => [
            'client_id' => 'c47be01f75872d14',
            'client_secret' => 'HfYU0XkNJ+XYHcTXukIzJ1SbjTrUX2IhL4r48DxJnq3B1fHq3CgS/SvzHQtB2PeS',
            'code' => '869a9cf13afb27f3199592f3d0dec6ad',
            'grant_type' => "authorization_code"
        ]
    ]);


});
