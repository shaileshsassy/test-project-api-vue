<?php

$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => "http://api-demo.seekom.com/properties-metadata",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_POSTFIELDS => "{\n\t\"city_id\": 48019,\n\t\"category_id\": 2\n}",
    CURLOPT_HTTPHEADER => array(
        "Authorization: Bearer pOCNZ0G9lm6foM3DO95aZY1n/nopc69PNCWz5tjyRdUJ3QADB9wFy0JUqiYiYYzb",
        "Cache-Control: no-cache",
        "Postman-Token: cd4ef8ff-7f5f-48e4-a93a-50bd278ffe75"
    ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
    echo "cURL Error #:" . $err;
} else {
    echo $response;
}